from django import forms
from django.db import models
from .models import StatusModel

class StatusForm(forms.ModelForm):
    class Meta :
        model = StatusModel
        fields = {"status"}
        widgets = {
            'status' : forms.Textarea(
                attrs = {
                    'class': 'form-control',
                    'placeholder': "What's Happening?",
                }
            ),
        }