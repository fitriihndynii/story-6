from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve, reverse
from django.utils import timezone
from .views import *
from .models import StatusModel
from .forms import StatusForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
import time

# Create your tests here.
class UnitTest(TestCase):
    def test_apakah_ada_url_status(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 200)

    def test_apakah_html_yang_digunakan_benar(self):
        response = Client().get('')
        self.assertTemplateUsed('status.html')

    def test_apakah_fungsi_status_dijalankan(self):
        response = resolve(reverse('status'))
        self.assertEqual(response.func, status)

    def test_hello(self):
        response = Client().get('')
        content = response.content.decode('utf-8')
        self.assertIn("Hello, How Are You?", content)

    def test_apakah_terdapat_model_StatusModel(self):
        s = StatusModel.objects.create(status = "Suatu String",date=timezone.now())
        self.assertEqual(str(s), "Suatu String")

    def test_apakah_form_dapat_dibuat_dan_tersimpan_di_model(self):
        data = { 'status' : 'Status 1'}
        status_form = StatusForm(data=data)
        self.assertTrue(status_form.is_valid())
        status_form.save()
        kumpulan_status = StatusModel.objects.all()
        self.assertEqual(kumpulan_status.count(),1)

    def test_form_tidak_valid(self):
        data = { 'status' : ''}
        status_form = StatusForm(data=data)
        self.assertFalse(status_form.is_valid())

    def test_menyimpan_request_POST(self):
        response = self.client.post('', data={'status':'String Status', 'date':'2019-11-03TI10:25'})
        self.assertEqual(response.status_code, 302)
        response_baru = self.client.get('/')
        html_response = response_baru.content.decode('utf-8')
        self.assertIn('String Status', html_response)

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        super().setUp()
        chrome_options = webdriver.ChromeOptions()
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        chrome_options.add_argument("--disable-dev-shm-usage")
        self.selenium = webdriver.Chrome(chrome_options=chrome_options)

    def tearDown(self):
        self.selenium.quit()
        super().tearDown()

    def test_memasukkan_status(self):
        self.selenium.get('%s%s' % (self.live_server_url, '/'))
        time.sleep(3)
        status = self.selenium.find_element_by_name('status')
        submit = self.selenium.find_element_by_id('submit')

        status_message = 'Story 6 PPW'
        status.send_keys(status_message)
        submit.click()
        time.sleep(5)
    