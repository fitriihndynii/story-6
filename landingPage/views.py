from django.shortcuts import render, redirect
from .models import StatusModel
from .forms import StatusForm

# Create your views here.
def status(request):
    data = StatusModel.objects.all().order_by('date')
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect('status')
    else:
        form = StatusForm()

    content = {'data':data,
                'form':form}
    return render(request, 'status.html', content)